#ifndef AEVOL_LOGGER_H
#define AEVOL_LOGGER_H

#include <string>
#include <list>
#include <chrono>

enum Category {
	SELECTION,
	TOTAL,
	SEARCH_START_RNA,
	DNA_TO_RNA,
	RNA_TO_PROTEIN,
	COMPUTE_PROTEIN,
	PROTEIN_TO_PHENOTYPE
};

const unsigned int CATEGORY_NUMBER = 7;

class Logger {
public:
	void logTimeSpent(Category cat, const std::chrono::high_resolution_clock::time_point & time_ref);
	void flush(const std::string & fileName = "logger.csv");

private:
	void flushLog(std::ofstream & output);
	std::list<long> logs [CATEGORY_NUMBER];
};

#endif  //AEVOL_LOGGER_H
