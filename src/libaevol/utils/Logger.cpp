#include "Logger.h"

#include <fstream>

using namespace std;

const char * CATEGORY_NAMES [] = {
		"SELECTION",
		"TOTAL",
		"SEARCH_START_RNA",
		"DNA_TO_RNA",
		"RNA_TO_PROTEIN",
		"COMPUTE_PROTEIN",
		"PROTEIN_TO_PHENOTYPE"
};

void Logger::logTimeSpent(Category cat, const chrono::high_resolution_clock::time_point & timeRef)
{
	auto currentTime = chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<chrono::microseconds>( currentTime - timeRef ).count();
	auto & log = logs[cat];
	log.insert(log.end(), duration);
}

void Logger::flush(const std::string &fileName)
{
	ofstream output(fileName);
	for(unsigned int i = 0; i < CATEGORY_NUMBER; ++i)
		output << CATEGORY_NAMES[i] << ',';
	output << endl;
	flushLog(output);
}

void Logger::flushLog(ofstream & output)
{
	list<long>::const_iterator iterators [CATEGORY_NUMBER];
	for(unsigned int i = 0; i < CATEGORY_NUMBER; ++i)
		iterators[i] = logs[i].cbegin();

	unsigned int ended = 0;
	while(ended < CATEGORY_NUMBER)
	{
		ended = 0;
		for(unsigned int i = 0; i < CATEGORY_NUMBER; ++i)
		{
			if(iterators[i] != logs[i].cend())
				output << *(iterators[i]++);
			else
				++ended;
			output << ',';
		}
		output << endl;
	}
}